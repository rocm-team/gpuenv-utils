# Author: Christian Kastner <ckk@kvr.at>
# License: MIT

# pylint: disable=unspecified-encoding


"""AMD GPU monitor and access arbiter.

Monitors GPU health, and serializes access by means of an advisory locking
mechanism.

This should probably not be used directly. Instead, an instance of
ResourceManager should be managed by a service that exposes its interface to
clients.
"""


import dataclasses
import logging
import enum
import subprocess
import threading
import time

import psutil


@dataclasses.dataclass(frozen=True)
class GPU:
    """Encapsulate info about a GPU on the system."""

    slot_id: str
    model: str | None = None
    driver: str | None = None
    vfio_id: int | None = None
    render_id: str | None = None


class GPUState(enum.StrEnum):
    """Possible states of a GPU."""

    FREE = "FREE"
    LOCKED = "LOCKED"
    DEAD = "DEAD"


class SlotError(Exception):
    """Raised when a non-managed slot is referenced."""

    def __init__(self, message, slot_id):
        super().__init__(message, slot_id)
        self.message = message
        self.slot_id = slot_id


class ResourceManager:
    """Abstraction for monitoring and arbitrating access to resources.

    When locking, if a lock was requested for a specific PID, then the lock
    will be implicitly released on termination of that process.

    Conversely, if any process (meaning no matter who or what started it) is
    discovered to be using the device, then a lock will automatically be
    granted to that PID. (This currently only works with VFIO-backed devices.)

    Devices are initially assumed to be free.

    Parameters
    ----------
    slot_ids : list[str]
        List of PCI slot IDs to manage. They will be normalized, meaning the
        domain (usually ``0000``) will be prepended, if missing.

    Attributes
    ----------
    gpus : dict[str, GPU]
        Dictionary of GPUs managed by this ResourceManager, indexed by
        normalized slot ID.

    Examples
    --------

    >>> mgr = ResourceManager(["03:00.0", "04:00.0"])
    """

    def __init__(self, slot_ids: list[str]) -> None:
        # Indexed by normalized slot ID
        self.gpus: dict[str, GPU] = {}
        # For methods that should operate atomically
        self._rlock = threading.RLock()
        # The normalized slot ID includes the domain (usually 0000)
        # We map the GPU from both the simple and the normalized name
        self._norm_slot_ids: dict[str, str] = {}

        # We track states in various objects here, instead of just one, because
        # each state can have additional metadata. This should be consolidated
        # at some point, once we know more what data we actually want.
        # Maps advisory locks to PIDs owning them
        self._locked: dict[str, int | None] = {}
        # Idle resources could be turned off
        self._idle_since: dict[str, float] = {}
        # Bring out your dead!
        self._dead: set[str] = set()
        self._dead_markers_ignore = self._load_dead_markers_ignore()

        for slot_id in slot_ids:
            try:
                gpu = self._create_gpu(slot_id)
            except KeyError as err:
                logging.critical("No device with slot ID: %s", slot_id)
                raise SlotError("No such device", slot_id) from err
            logging.info("Managing GPU: %s", gpu)
            self.gpus[gpu.slot_id] = gpu
            # With domain
            self._norm_slot_ids[gpu.slot_id] = gpu.slot_id
            # Without domain
            self._norm_slot_ids[gpu.slot_id.partition(":")[2]] = gpu.slot_id

    def _create_gpu(self, slot_id: str) -> GPU:
        """Create a GPU instance.

        Retrieves GPU metadata from various parts of the system.
        """
        # The formalized, machine-readable output of lspci
        cmd = ["lspci", "-s", slot_id, "-Dvmm"]
        lines = subprocess.check_output(cmd, text=True).split("\n")
        mrrecs = {k: v.strip() for k, _, v in (line.partition(":") for line in lines)}

        # The non-formalized, more detailed output of lspci
        cmd = ["lspci", "-s", slot_id, "-vv"]
        lines = subprocess.check_output(cmd, text=True).split("\n")
        recs = {
            k.strip(): v.strip() for k, _, v in (line.partition(":") for line in lines)
        }

        gpu = GPU(
            slot_id=mrrecs["Slot"],
            model=mrrecs["Device"],
            driver=recs["Kernel driver in use"],
            vfio_id=int(recs["IOMMU group"])
            if recs["Kernel driver in use"] == "vfio-pci"
            else None,
        )
        return gpu

    def _load_dead_markers_ignore(self) -> list[str]:
        """Load strings that negate the effect of a deadness marker."""
        try:
            with open("/etc/gpuenv-utils/markers.notdead", "r") as fobj:
                return fobj.read().split("\n")
        except FileNotFoundError:
            return []

    def normalize(self, slot_id: str) -> str:
        """Return the normalized slot ID.

        Meaning that the domain (usually ``0000``) will be prepended, if
        missing.
        """
        try:
            return self._norm_slot_ids[slot_id]
        except KeyError as err:
            raise SlotError("No such device", slot_id) from err

    def list_all(self, vfio: bool | None = None) -> list[str]:
        """Return the list of normalized slot IDs known to this manager.

        Parameters
        ----------
        vfio : bool | None
            If None, return all devices. If True, return all devices assigned
            to vfio-pci. If False, return all devices assigned to amdgpu.

        Returns
        -------
        list[str]
            Sorted list of normalized slot IDs.
        """
        gpus = self.gpus.values()
        match vfio:
            case None:
                return sorted(gpu.slot_id for gpu in gpus)
            case True:
                return sorted(gpu.slot_id for gpu in gpus if gpu.vfio_id is not None)
            case False:
                return sorted(gpu.slot_id for gpu in gpus if gpu.vfio_id is None)

    def get_state(self, slot_id: str) -> GPUState:
        """Return the state of a GPU."""
        # Racy, but what can you do. A lock won't help
        if self.is_dead(slot_id):
            return GPUState.DEAD
        if self.is_locked(slot_id):
            return GPUState.LOCKED
        return GPUState.FREE

    def is_free(self, slot_id: str) -> bool:
        """Test whether a particular GPU is ready for use.

        Currently, this simply checks that a device is neither locked nor dead.
        """
        return not (self.is_locked(slot_id) or self.is_dead(slot_id))

    def is_free_multi(self, slot_ids: list[str]) -> bool:
        """Test whether a list of GPUs are all ready for use."""
        return all(self.is_free(slot_id) for slot_id in slot_ids)

    def is_locked(self, slot_id: str) -> bool:
        """Test whether a particular GPU is locked by someone.

        If the device was locked by a particular PID, then the lock will be
        released if that process is no longer around.
        """
        slot_id = self.normalize(slot_id)
        if slot_id not in self._locked:
            return False

        with self._rlock:
            pid = self._locked[slot_id]
            if pid and not psutil.pid_exists(pid):
                logging.debug(
                    "GPU %s: PID %s has disappeared, releasing lock.", slot_id, pid
                )
                self.release(slot_id)
                return False
        return True

    def is_dead(self, slot_id: str) -> bool:
        """Test whether a particular GPU is in an irrecoverable state.

        Currently, this relies entirely on ``dmesg`` output.
        """
        slot_id = self.normalize(slot_id)
        if slot_id in self._dead:
            return True

        if self._check_dead(slot_id):
            self.mark_dead(slot_id)
            return True
        return False

    def _check_dead(self, slot_id: str) -> bool:
        """Check for clues that a GPU has entered an irrecoverable state."""
        messages = subprocess.check_output(["dmesg"], text=True).split("\n")
        messages = [m for m in messages if f"vfio-pci {slot_id}" in m]
        dead_markers = [
            "device inaccessible",
            "Refused to change power state",
            "Unable to change power state",
        ]
        for line in messages:
            if any(ignore in line for ignore in self._dead_markers_ignore):
                continue
            if any(marker in line for marker in dead_markers):
                return True
        return False

    def lock(self, slot_id: str, pid: int | None = None) -> bool:
        """Lock a GPU for exclusive use.

        Parameters
        ----------
        slot_id : str
            PCI slot ID to lock.
        pid : int | None
            Optionally register this PID as holding the lock. If present, then
            the lock will be automatically released when that process
            terminates.

        Returns
        -------
        bool
            True if the GPU could be locked for exclusive use, otherwise False.
        """
        with self._rlock:
            result = self._lock(slot_id, pid)
            if result:
                logging.debug("Locked GPU: %s", slot_id)
                return True
            return False

    def lock_multi(self, slot_ids: list[str], pid: int | None = None) -> bool:
        """Lock a list of GPUs for exclusive use.

        Devices must all have the same type (regular or VFIO) for a lock to
        succeed.

        If one or more locks fail, all acquired locks will be released again.

        Parameters
        ----------
        slot_ids : str
            List of PCI slot IDs to lock.
        pid : int | None
            Optionally register this PID as holding the lock. If present, then
            the lock will be automatically released when that process
            terminates.

        Returns
        -------
        bool
            True if all GPUs could be locked for exclusive use, otherwise False.
        """
        gpus = [self.gpus[self._norm_slot_ids[sid]] for sid in slot_ids]
        vfio = [gpu.vfio_id is not None for gpu in gpus]
        if not all(vfio) and any(vfio):
            # Can't mix regular and VFIO
            return False

        acquired = set()
        with self._rlock:
            for slot_id in slot_ids:
                if self._lock(slot_id, pid):
                    acquired.add(slot_id)
                else:
                    break
            if acquired == set(slot_ids):
                logging.debug("Locked GPUs: %s", " ".join(acquired))
                return True
            # At least one lock failed
            for slot_id in acquired:
                self.release(slot_id)
            return False

    @staticmethod
    def _maybe_find_holders(path: str) -> list[int]:
        """Try to get a list of PIDs holding a fileystem object open.

        The object is identified by path, and can be anything that lsof
        understands (files, devices, etc.).

        Will return an empty list if no holders were found, or lookup of
        holders failed for any reason -- hence the "maybe".
        """
        try:
            out = subprocess.check_output(["lsof", path], text=True)
        except subprocess.CalledProcessError:
            return []

        # Skip the header line
        lines = out.split("\n")[1:]
        # PID is in the second column
        return [int(line.split()[1]) for line in lines if line.strip()]

    def _lock(self, slot_id: str, pid: int | None = None) -> bool:
        """Actual locking logic. Assumes that caller has acquired RLock."""
        slot_id = self.normalize(slot_id)
        if self.is_locked(slot_id):
            return False
        if self.is_dead(slot_id):
            return False

        # Either we're going to lock it, or we're going to discover another
        # process using it, so remove any idle info
        self._idle_since.pop(slot_id, None)

        gpu = self.gpus[slot_id]
        if gpu.vfio_id:
            device_path = f"/dev/vfio/{gpu.vfio_id}"
            # Device should be free, but let's see if a process has opened it
            try:
                with open(device_path):
                    # All good, we can lock it for us
                    pass
            except OSError:
                # Somebody else is using it. See if we can find out, who
                pids = self._maybe_find_holders(device_path)
                if not pids:
                    self._locked[slot_id] = None
                    return False
                # There could be more than one process accessing, but what can
                # we do
                self._locked[slot_id] = pids[0]
                return False

        self._locked[slot_id] = pid
        return True

    def get_lock_pid(self, slot_id: str) -> int | None:
        """Get the PID of a process owning a lock on a GPU.

        Parameters
        ----------
        slot_id : str
            PCI slot ID to lock.
            terminates.

        Returns
        -------
        pid : int | None
            The PID of the process holding the lock, if one was registered,
            otherwise None.
        """
        slot_id = self.normalize(slot_id)
        with self._rlock:
            return self._locked.get(slot_id)

    def release(self, slot_id: str) -> None:
        """Release a lock on a GPU."""
        # Shouldn't need a lock
        slot_id = self.normalize(slot_id)
        if slot_id in self._locked:
            del self._locked[slot_id]
            self._idle_since[slot_id] = time.time()
            logging.debug("GPU %s released", slot_id)

    def release_multi(self, slot_ids: list[str]) -> None:
        """Release lock on a list of GPUs."""
        for slot_id in slot_ids:
            self.release(slot_id)

    def mark_dead(self, slot_id: str) -> None:
        """Mark a GPU as dead."""
        slot_id = self.normalize(slot_id)
        # Technically, these don't need a lock, but we want to prohibit access
        # as soon as possible
        with self._rlock:
            self._dead.add(slot_id)
            self._locked.pop(slot_id, None)
            # A dead GPU is not idle, meaning a check will always return 0
            self._idle_since.pop(slot_id, None)
        logging.info("GPU %s marked as dead.", slot_id)

    def scan(self) -> None:
        """Scan all devices to refresh their state."""
        logging.info("Refreshing state of all devices...")
        for gpu in self.gpus.values():
            if self.lock(gpu.slot_id):
                self.release(gpu.slot_id)
        logging.info("Refresh complete.")

    def idle_sec(self, slot_id: str) -> int:
        """Return the number of seconds a GPU has been idle.

        Returns 0 if the GPU is not idle.
        """
        with self._rlock:
            slot_id = self.normalize(slot_id)
            since = self._idle_since.get(slot_id)
            if since is None:
                # Check for implicit release of a lock
                self.is_locked(slot_id)
                since = self._idle_since.get(slot_id)
            sec = round(time.time() - since) if since else 0
            logging.debug("GPU %s idle for %ss", slot_id, sec)
            return sec

    def idle_sec_all(self) -> int:
        """Return the number of seconds all GPUs have been idle.

        Returns 0 if any GPU is not idle, otherwise the smallest of their idle
        times.
        """
        with self._rlock:
            return min(self.idle_sec(gpu.slot_id) for gpu in self.gpus.values())
