# Author: Christian Kastner <ckk@kvr.at>
# License: MIT


"""A multi-threaded, socket-listening service wrapping a ResourceManager.

The service can be started either directly, or via systemd.
"""


from concurrent.futures import ThreadPoolExecutor
import inspect
import logging
import logging.handlers
import os
import shlex
import signal
import socket
import struct
import threading
import types
from types import UnionType
from typing import get_args, get_origin, Any, Tuple

from manager import ResourceManager, SlotError


class ConfigError(Exception):
    """Raised when the server is configured incorrectly."""

    def __init__(self, message):
        super().__init__(message)


class ResourceManagerService:
    """Process ResourceManager commands received on a UNIX socket.

    This service waits for connections on a socket, and launches threads to
    process the connections. In that thread, a single command is read from the
    socket, executed against the wrapped ResourceManager, and the result sent
    back.

    Commands and replies are simple one-line strings. See the parse_command()
    function for the expected syntax.

    Either ``systemd`` or ``socket_path`` must be specified.

    Parameters
    ----------
    slot_ids : list[str]
        List of slot_ids to manage.
    systemd : bool
        Whether the service is socket-activated by systemd.
    socket_path : str | None
        If not socket-activated by systemd, this is the path to the socket to
        listen on.
    scan : bool
        Invoke scan() on startup to initialize the state of all devices,
        instead of assuming that they are all free.

    Attributes
    ----------
    socket_path : str
        The stored value of the socket_path parameter. If this is None, then
        this indicates that the service was started by systemd.
    """

    # We don't expect to be used interactively, so timeout is reasonably low
    SOCKET_TIMEOUT = 5
    SOCKET_BUFSIZ = 1024

    def __init__(
        self,
        slot_ids: list[str],
        systemd: bool = False,
        socket_path: str | None = None,
        scan: bool = True,
    ) -> None:
        self._stop_event = threading.Event()
        signal.signal(signal.SIGINT, self._signal_stop)
        signal.signal(signal.SIGTERM, self._signal_stop)

        self.socket_path = socket_path
        if systemd and socket_path is not None:
            raise ConfigError("Cannot use both systemd and socket_path")
        if not systemd and socket_path is None:
            raise ConfigError("Need either systemd or socket_path")
        self._socket: socket.socket | None = None
        self._manager = ResourceManager(slot_ids)

        # Read all public methods from the ResourceManager, and map their
        # parameter signature as a dict
        methods = {
            k: v
            for (k, v) in inspect.getmembers(self._manager, predicate=inspect.ismethod)
            if not k.startswith("_")
        }
        self._cmdparams = {k: inspect.signature(methods[k]).parameters for k in methods}

        if scan:
            self._manager.scan()

    def run(self) -> None:
        """Bind to the socket and start listening."""
        if self.socket_path is None:
            # systemd passes us fd 3
            self._socket = socket.fromfd(
                3, family=socket.AF_UNIX, type=socket.SOCK_STREAM
            )
        else:
            self._socket = socket.socket(family=socket.AF_UNIX, type=socket.SOCK_STREAM)
            self._socket.bind(self.socket_path)
        self._socket.listen()
        logging.info("Listening...")

        futures = []
        with ThreadPoolExecutor(max_workers=20) as executor:
            # To enable graceful stop
            self._socket.settimeout(1)
            while True:
                try:
                    conn, _ = self._socket.accept()
                    futures.append(executor.submit(self.process_request, conn))
                except KeyboardInterrupt:
                    self._stop_event.set()
                    break
                except socket.timeout:
                    if self._stop_event.is_set():
                        break
                futures = [f for f in futures if not f.done()]
        self._socket.close()
        if self.socket_path:
            os.remove(self.socket_path)

    # pylint: disable-next=unused-argument
    def _signal_stop(self, signum: int, frame: types.FrameType | None = None) -> None:
        logging.info("Received signal to stop")
        self._stop_event.set()

    def stop(self):
        """Shutdown gracefully."""
        logging.info("Received request to stop")
        self._stop_event.set()

    # pylint: disable-next=too-many-branches
    def parse_command(self, args: list[str]) -> Tuple[str, dict[str, Any]]:
        """Parse method name and kwargs from a positional argument list.

        The first element of the argument array is interpreted as the name of
        the method to call. All remaining arguments are matched (by position)
        to the arguments of that method's signature.

        Arguments can be quoted. foo "bar baz" would be interpreted as a call
        to foo("bar baz"). Where lists are expected, list elements should be
        comma-separated, for example "foo elem1,elem2,elem3".

        The supported method names are all public methods of the
        ResourceManager class. Supported argument types are:

            * int | None, str | None, bool | None
            * int, str
            * list[int], list[str]

        Parameters
        ----------
        args : list[str]
            Argument list.

        Returns
        -------
        (methodname, kwargs) : Tuple[str, dict[str,Any]]
            A tuple of method name, and a dictionary of arguments for that
            method.

        Raises
        ------
        NameError
            When the requested method does not exist.
        ValueError
            When any of the arguments don't match the type required
        IndexError
            When fewer than required arguments are supplied.
        NotImplementedError
            When the function signature contains types that are not supported
            yet.
        """
        func = args[0]
        if func not in self._cmdparams:
            raise NameError(f"unknown command: {func}")

        # Iterate over all positional arguments and try to match them to the
        # function's signature
        idx = 0
        kwargs: dict[str, Any] = {}
        for name in self._cmdparams[func]:
            idx += 1
            annot = self._cmdparams[func][name].annotation

            # Optional arguments
            if get_origin(annot) is UnionType and type(None) in get_args(annot):
                try:
                    arg = args[idx]
                except IndexError:
                    # Out of arguments
                    break
                if get_args(annot)[0] is int:
                    kwargs[name] = int(arg)
                elif get_args(annot)[0] is str:
                    kwargs[name] = arg
                elif get_args(annot)[0] is bool:
                    match arg:
                        case "True":
                            kwargs[name] = True
                        case "False":
                            kwargs[name] = False
                        case "None":
                            kwargs[name] = None
                        case _:
                            raise ValueError(f"Illegal value {arg}")
                else:
                    raise NotImplementedError(f"Unsupported argument type: {annot}")
                continue

            # Throws IndexError if fewer arguments than required are supplied
            arg = args[idx]

            if get_origin(annot) is list:
                if get_args(annot)[0] is int:
                    kwargs[name] = [int(a) for a in arg.split(",")]
                elif get_args(annot)[0] is str:
                    kwargs[name] = arg
                    kwargs[name] = arg.split(",")
                else:
                    raise NotImplementedError(f"Unsupported argument type: {annot}")
            elif annot is int:
                kwargs[name] = int(arg)
            elif annot is str:
                if "," in arg:
                    raise ValueError("Can't use list here")
                kwargs[name] = arg
            else:
                raise NotImplementedError(f"Unsupported argument type: {annot}")

        if len(args) > idx + 1:
            raise ValueError("Too many arguments")
        return func, kwargs

    # pylint: disable-next=too-many-statements
    def process_request(self, conn: socket.socket) -> None:
        """Process a command sent by a client.

        Parameters
        ----------
        conn: socket.socket
            The client socket as the result of an accept().
        """
        creds = conn.getsockopt(
            socket.SOL_SOCKET, socket.SO_PEERCRED, struct.calcsize("3i")
        )
        pid, uid, gid = struct.unpack("3i", creds)
        logging.debug("PID=%s Connection from UID=%s GID=%s", pid, uid, gid)

        conn.settimeout(self.SOCKET_TIMEOUT)
        try:
            request = conn.recv(self.SOCKET_BUFSIZ).decode()
        except socket.timeout:
            logging.debug("PID=%s Request timed out", pid)
            conn.sendall(b"408 Timed out")
            return

        if not request.strip():
            logging.debug("PID=%s Empty request", pid)
            conn.sendall(b"400 Empty request")
            conn.close()
            return
        logging.debug("PID=%s request: %s", pid, request)

        args = shlex.split(request)
        try:
            method_name, kwargs = self.parse_command(args)
        except NameError:
            logging.error("PID=%s Unsupported method: %s", pid, args[0])
            conn.sendall(b"400 Unsupported method")
            conn.close()
            return
        except (IndexError, ValueError, NotImplementedError):
            logging.error("PID=%s Malformed command: %s", pid, request)
            conn.sendall(b"400 Malformed command")
            conn.close()
            return
        except Exception as err:  # pylint: disable=broad-exception-caught
            logging.error("PID=%s Uncaught exception: %s", pid, err)
            conn.sendall(b"500 Uncaught exception processing command")
            conn.close()
            return

        try:
            method = getattr(self._manager, method_name)
            result = method(**kwargs)
        except SlotError as err:
            logging.error("PID=%s Unknown slot: %s", pid, err.slot_id)
            conn.sendall(b"405 Unknown slot")
            conn.close()
            return
        except Exception as err:  # pylint: disable=broad-exception-caught
            logging.error("PID=%s Uncaught exception: %s", pid, err)
            conn.sendall(b"500 Uncaught exception processing command")

        logging.debug("PID=%s result: %s", pid, result)
        if result is None:
            conn.sendall(b"200")
        elif isinstance(result, list):
            conn.sendall(b"200 " + ",".join(result).encode())
        else:
            conn.sendall(b"200 " + str(result).encode())
        conn.close()
