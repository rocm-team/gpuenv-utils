Source: gpuenv-utils
Section: admin
Homepage: https://salsa.debian.org/rocm-team/gpuenv-utils
Priority: optional
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/rocm-team/gpuenv-utils.git
Vcs-Browser: https://salsa.debian.org/rocm-team/gpuenv-utils
Maintainer: Debian ROCm Team <debian-ai@lists.debian.org>
Uploaders: Christian Kastner <ckk@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               help2man,
               python3-all,
               python3-psutil,
               python3-pytest
Rules-Requires-Root: no

Package: gpuenv-server
Architecture: all
Depends: python3-psutil, ${misc:Depends}, ${python3:Depends}
Description: Services for facilitating cooperative access to GPUs
 This service monitors and arbitrates access to GPUs. The primary intent is to
 let various concurrently running test workers, build workers, and porter box
 environments have exclusive control over one or more GPUs for the duration of
 their work.
 .
 This currently provides the following components:
 .
  * gpuenv-server: A service that listens for client requests
  * gpuenv-cli: A minimal utility for interacting with the server from within a
    shell.
