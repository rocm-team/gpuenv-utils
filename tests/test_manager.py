# Author: Christian Kastner <ckk@kvr.at>
# License: MIT

# pylint: disable=unspecified-encoding


"""Tests for the ResourceManager component."""


import builtins
import dataclasses
import multiprocessing
from pathlib import Path
import subprocess
import sys
import time
from unittest.mock import Mock, mock_open

import pytest
import psutil

sys.path.insert(0, "/usr/share/gpuenv-server/lib")
sys.path.insert(0, (Path(__file__).parent.parent / "lib").as_posix())
# pylint: disable-next=wrong-import-position
from manager import GPU, GPUState, ResourceManager, SlotError


# These paths hold mock implementations of lspci, dmesg, lsof
mock_cmds = Path(__file__).parent / "mock_cmds"
mock_cmds_reg = (mock_cmds / "reg").as_posix()
mock_cmds_vfio = (mock_cmds / "vfio").as_posix()
mock_cmds_mixed = (mock_cmds / "mixed").as_posix()
mock_cmds_vfio_bad = (mock_cmds / "vfio" / "bad").as_posix()
mock_cmds_vfio_empty = (mock_cmds / "vfio" / "empty").as_posix()

# Two devices and various combinations of IDs
mock_slot_ids = ["06:00.0", "11:00.0"]
mock_norm_slot_ids = ["0000:06:00.0", "0000:11:00.0"]
mock_slot_ids_06 = [mock_slot_ids[0], mock_norm_slot_ids[0]]
mock_slot_ids_11 = [mock_slot_ids[1], mock_norm_slot_ids[1]]

# The devices we expect to see, given our mock data
gpu_06 = GPU(
    slot_id="0000:06:00.0",
    model="Navi 23 [Radeon RX 6650 XT / 6700S / 6800S]",
    driver="amdgpu",
)
gpu_11 = GPU(
    slot_id="0000:11:00.0",
    model="Navi 24 [Radeon RX 6400/6500 XT/6500M]",
    driver="amdgpu",
)
gpu_06_vfio = dataclasses.replace(gpu_06, driver="vfio-pci", vfio_id=25)
gpu_11_vfio = dataclasses.replace(gpu_11, driver="vfio-pci", vfio_id=35)


@pytest.fixture(name="mgr")
def fixture_mgr(monkeypatch):
    """A mock ResourceManager instance with two non-VFIO devices."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_reg)
        return ResourceManager(mock_slot_ids)


@pytest.fixture(name="mgr_vfio")
def fixture_mgr_vfio(monkeypatch):
    """A mock ResourceManager instance with two VFIO devices."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_vfio)
        return ResourceManager(mock_slot_ids)


@pytest.fixture(name="mgr_mixed")
def fixture_mgr_mixed(monkeypatch):
    """A mock ResourceManager instance with one regular and one VFIO device."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_mixed)
        return ResourceManager(mock_slot_ids)


@pytest.mark.parametrize(
    "slot_ids,expected",
    [
        (["06:00.0"], [gpu_06]),
        (["0000:06:00.0"], [gpu_06]),
        (["11:00.0"], [gpu_11]),
        (["06:00.0", "11:00.0"], [gpu_06, gpu_11]),
    ],
)
def test_init_regular(monkeypatch, slot_ids, expected):
    """ResourceManager initialization with regular GPUs."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_reg)
        mgr = ResourceManager(slot_ids)
    expected = {g.slot_id: g for g in expected}
    assert mgr.gpus == expected


@pytest.mark.parametrize(
    "slot_ids,expected",
    [
        (["06:00.0"], [gpu_06_vfio]),
        (["0000:06:00.0"], [gpu_06_vfio]),
        (["11:00.0"], [gpu_11_vfio]),
        (["06:00.0", "11:00.0"], [gpu_06_vfio, gpu_11_vfio]),
    ],
)
def test_init_vfio(monkeypatch, slot_ids, expected):
    """ResourceManager initialization with VFIO-assigned GPUs."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_vfio)
        mgr = ResourceManager(slot_ids)
    expected = {g.slot_id: g for g in expected}
    assert mgr.gpus == expected


@pytest.mark.parametrize(
    "slot_ids,expected",
    [
        (["06:00.0"], [gpu_06]),
        (["0000:06:00.0"], [gpu_06]),
        (["11:00.0"], [gpu_11_vfio]),
        (["06:00.0", "11:00.0"], [gpu_06, gpu_11_vfio]),
    ],
)
def test_init_mixed(monkeypatch, slot_ids, expected):
    """ResourceManager initialization with both regular and VFIO-assigned GPUs."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_mixed)
        mgr = ResourceManager(slot_ids)
    expected = {g.slot_id: g for g in expected}
    assert mgr.gpus == expected


def test_init_nonexisting_raises(monkeypatch):
    """ResourceManager initialization fails on unknown slot ID."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_reg)
        with pytest.raises(SlotError):
            ResourceManager(["99999:00.0"])


def test_normalize_slot(mgr):
    """Normalizing a slot ID."""
    assert mgr.normalize(mock_slot_ids_06[0]) == mock_slot_ids_06[1]


def test_normalize_nonexisting_raises(mgr):
    """Normalizing a slot ID."""
    with pytest.raises(SlotError):
        mgr.normalize("99999:00.0")
        mgr.is_free("99999:00.0")


@pytest.mark.parametrize("slot_ids", [mock_slot_ids, mock_norm_slot_ids])
def test_list_all(monkeypatch, mgr, slot_ids):
    """Listing all devices known to the manager."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_reg)
        mgr = ResourceManager(slot_ids)
        assert mgr.list_all() == mock_norm_slot_ids


def test_list_all_regular(monkeypatch, mgr, mgr_mixed):
    """Listing all devices known to the manager, regular devices only."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_reg)
        assert mgr.list_all(vfio=False) == mock_norm_slot_ids
        assert mgr.list_all(vfio=True) == []
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_mixed)
        assert mgr_mixed.list_all(vfio=False) == [mock_norm_slot_ids[0]]


def test_list_all_vfio(monkeypatch, mgr_vfio, mgr_mixed):
    """Listing all devices known to the manager, VFIO devices only."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_vfio)
        assert mgr_vfio.list_all(vfio=True) == mock_norm_slot_ids
        assert mgr_vfio.list_all(vfio=False) == []
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_vfio)
        assert mgr_mixed.list_all(vfio=True) == [mock_norm_slot_ids[1]]


@pytest.mark.parametrize("slot_id", mock_slot_ids_06)
def test_is_dead(monkeypatch, mgr_vfio, slot_id):
    """Devices that had entered an irrecoverable state detected as dead."""
    with monkeypatch.context() as ctx:
        slot_id = mgr_vfio.normalize(slot_id)
        ctx.setenv("PATH", mock_cmds_vfio)
        assert not mgr_vfio.is_dead(slot_id)
        ctx.setenv("PATH", mock_cmds_vfio_bad)
        assert mgr_vfio.is_dead(slot_id)


@pytest.mark.parametrize("slot_id", mock_slot_ids_06)
def test_is_dead_ignore_marker(monkeypatch, slot_id):
    """Dead markers can be extended."""
    ignore = "device inaccessible\nRefused to change power state\nUnable to change power state"
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_vfio_bad)
        mgr_vfio = ResourceManager([slot_id])
        assert mgr_vfio.is_dead(slot_id)
        ctx.setattr(builtins, "open", mock_open(read_data=ignore))
        mgr_vfio = ResourceManager([slot_id])
        assert not mgr_vfio.is_dead(slot_id)


@pytest.mark.parametrize("slot_id", mock_slot_ids_06)
def test_mark_dead(monkeypatch, mgr, slot_id):
    """Devices manually marked as dead are detected as dead."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_vfio)
        assert not mgr.is_dead(slot_id)
        mgr.mark_dead(slot_id)
        assert mgr.is_dead(slot_id)


def test_is_locked(mgr):
    """Devices are not locked by default."""
    for slot_id in mock_slot_ids_06 + mock_slot_ids_11:
        assert not mgr.is_locked(slot_id)


@pytest.mark.parametrize("slot_ids", [mock_slot_ids, mock_norm_slot_ids])
def test_lock_regular(monkeypatch, mgr, slot_ids):
    """Locking of a regular (non-VFIO) device."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_reg)
        for slot_id in slot_ids:
            assert not mgr.is_locked(slot_id)
        mgr.lock(slot_ids[0])
        assert mgr.is_locked(slot_ids[0])
        for slot_id in slot_ids[1:]:
            assert not mgr.is_locked(slot_id)


@pytest.mark.parametrize("slot_id", mock_slot_ids_06)
def test_locks_exclusive_regular(monkeypatch, mgr, slot_id):
    """Cannot lock an already locked device."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_reg)
        assert mgr.lock(slot_id)
        assert not mgr.lock(slot_id)


@pytest.mark.parametrize("slot_id", mock_slot_ids_06)
def test_locks_exclusive_regular_withpid(monkeypatch, mgr, slot_id):
    """Cannot lock an already locked device (by PID)."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_reg)
        # Not a real PID, so we need to prevent implicit release of the lock
        ctx.setattr(psutil, "pid_exists", lambda x: True)
        assert mgr.lock(slot_id, pid=123123123123)
        assert not mgr.lock(slot_id)


@pytest.mark.parametrize("slot_ids", [mock_slot_ids, mock_norm_slot_ids])
def test_lock_free_vfio(monkeypatch, mgr_vfio, slot_ids):
    """Locking of a VFIO device not in use."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_vfio)
        # The device is assumed to be free, so we want open to succeed
        ctx.setattr(builtins, "open", mock_open())
        for slot_id in slot_ids:
            assert not mgr_vfio.is_locked(slot_id)

        mgr_vfio.lock(slot_ids[0])
        assert mgr_vfio.is_locked(slot_ids[0])
        assert mgr_vfio.get_lock_pid(slot_ids[0]) is None
        for slot_id in slot_ids[1:]:
            assert not mgr_vfio.is_locked(slot_id)

        # Not a real PID, so we need to prevent implicit release of the lock
        ctx.setattr(psutil, "pid_exists", lambda x: True)
        mgr_vfio.lock(slot_ids[1], 123123123123)
        for slot_id in slot_ids:
            assert mgr_vfio.is_locked(slot_id)
        assert mgr_vfio.get_lock_pid(slot_ids[1]) == 123123123123


@pytest.mark.parametrize("slot_id", mock_slot_ids_06)
def test_lock_occupied_vfio(monkeypatch, mgr_vfio, slot_id):
    """Locking of a VFIO device in use by some other process, lsof works."""
    with monkeypatch.context() as ctx:
        # Our mock lsof will produce 1234512345 as PID. This is not a real PID,
        # so we need to prevent implicit releases of locks
        ctx.setattr(psutil, "pid_exists", lambda x: True)
        # This will simulate some other process holding the device open
        ctx.setattr(builtins, "open", Mock(side_effect=OSError))
        ctx.setenv("PATH", mock_cmds_vfio)
        assert not mgr_vfio.is_locked(slot_id)
        assert not mgr_vfio.lock(slot_id)
        # Locking failed, and device is now considered locked by the PID
        # holding it open
        assert mgr_vfio.is_locked(slot_id)
        assert mgr_vfio.get_lock_pid(slot_id) == 1234512345


@pytest.mark.parametrize("slot_id", mock_slot_ids_06)
def test_lock_occupied_empty_lsof_vfio(monkeypatch, mgr_vfio, slot_id):
    """Locking of a VFIO device in use by some other process, lsof is empty."""
    with monkeypatch.context() as ctx:
        # This will simulate some other process holding the device open
        ctx.setattr(builtins, "open", Mock(side_effect=OSError))
        # This will simulate lsof producing an empty result
        ctx.setenv("PATH", mock_cmds_vfio_empty)
        assert not mgr_vfio.is_locked(slot_id)
        assert not mgr_vfio.lock(slot_id)
        # Locking failed, and device is now considered locked
        assert mgr_vfio.is_locked(slot_id)
        assert mgr_vfio.get_lock_pid(slot_id) is None


@pytest.mark.parametrize("slot_id", mock_slot_ids_06)
def test_lock_occupied_no_lsof_vfio(monkeypatch, mgr_vfio, slot_id):
    """Locking of a VFIO device in use by some other process, lsof fails."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_vfio)
        # This will simulate some other process holding the device open
        ctx.setattr(builtins, "open", Mock(side_effect=OSError))
        # is_dead is incompatible with the check_output mock below
        ctx.setattr(mgr_vfio, "is_dead", lambda x: False)
        # This will simulate lsof failing to execute
        ctx.setattr(
            subprocess,
            "check_output",
            Mock(side_effect=subprocess.CalledProcessError(returncode=1, cmd="<none>")),
        )
        assert not mgr_vfio.is_locked(slot_id)
        assert not mgr_vfio.lock(slot_id)
        # Locking failed, and device is now considered locked
        assert mgr_vfio.is_locked(slot_id)
        assert mgr_vfio.get_lock_pid(slot_id) is None


@pytest.mark.parametrize("slot_id", mock_slot_ids_06)
def test_lock_dead(monkeypatch, mgr, slot_id):
    """A dead device cannot be locked."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_vfio_bad)
        assert not mgr.lock(slot_id)


@pytest.mark.parametrize("slot_id", mock_slot_ids_06)
def test_release(monkeypatch, mgr, slot_id):
    """Releasing of a regular (non-VFIO) device."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_vfio)
        assert mgr.lock(slot_id)
        mgr.release(slot_id)
        assert not mgr.is_locked(slot_id)


@pytest.mark.parametrize("slot_id", mock_slot_ids_06)
def test_lock_implicit_release(monkeypatch, mgr, slot_id):
    """A disappearing owner process implicitly releases a lock."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_reg)
        proc = multiprocessing.Process(target=lambda: time.sleep(5))
        proc.start()
        mgr.lock(slot_id, proc.pid)
        assert mgr.is_locked(slot_id)
        assert mgr.get_lock_pid(slot_id) == proc.pid
        proc.kill()
        proc.join()
        assert not mgr.is_locked(slot_id)


@pytest.mark.parametrize("slot_id", mock_slot_ids_06)
def test_is_free_locked(monkeypatch, mgr, slot_id):
    """A locked device is not free."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_reg)
        assert mgr.is_free(slot_id)
        mgr.lock(slot_id)
        assert not mgr.is_free(slot_id)


@pytest.mark.parametrize("slot_id", mock_slot_ids_06)
def test_is_free_dead(monkeypatch, mgr, slot_id):
    """A dead device is not free."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_reg)
        assert mgr.is_free(slot_id)
        mgr.mark_dead(slot_id)
        assert not mgr.is_free(slot_id)


@pytest.mark.parametrize("slot_id", mock_slot_ids_06)
def test_get_state(monkeypatch, mgr, slot_id):
    """State command returns the state as a value."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_reg)
        assert mgr.get_state(slot_id) == GPUState.FREE
        mgr.lock(slot_id)
        assert mgr.get_state(slot_id) == GPUState.LOCKED
        mgr.release(slot_id)
        assert mgr.get_state(slot_id) == GPUState.FREE
        mgr.mark_dead(slot_id)
        assert mgr.get_state(slot_id) == GPUState.DEAD


@pytest.mark.parametrize("slot_ids", [mock_slot_ids, mock_norm_slot_ids])
def test_is_free_multi(monkeypatch, mgr, slot_ids):
    """If at least one device is locked, overall result not free."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_reg)
        assert mgr.is_free_multi(slot_ids)
        mgr.lock(slot_ids[0])
        assert not mgr.is_free_multi(slot_ids)
        mgr.release(slot_ids[0])
        assert mgr.is_free_multi(slot_ids)


@pytest.mark.parametrize("slot_ids", [mock_slot_ids, mock_norm_slot_ids])
def test_lock_multi(monkeypatch, mgr, slot_ids):
    """Locking multiple devices fails if at least one device cannot be locked."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_reg)
        # Not a real PID, so we need to prevent implicit release of the lock
        ctx.setattr(psutil, "pid_exists", lambda x: True)
        mgr.lock(slot_ids[0], 123123123123)
        assert not mgr.lock_multi(slot_ids)
        assert mgr.is_locked(slot_ids[0])
        for slot_id in slot_ids[1:]:
            assert not mgr.is_locked(slot_id)
        mgr.release(slot_ids[0])
        assert mgr.lock_multi(slot_ids, 123123123123)
        for slot_id in slot_ids:
            assert mgr.get_lock_pid(slot_id) == 123123123123


@pytest.mark.parametrize("slot_ids", [mock_slot_ids, mock_norm_slot_ids])
def test_lock_multi_mixed_raises(monkeypatch, mgr_mixed, slot_ids):
    """Locking multiple devices fails if at least one device cannot be locked."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_mixed)
        # Not a real PID, so we need to prevent implicit release of the lock
        ctx.setattr(psutil, "pid_exists", lambda x: True)
        # We want the vfio devices to look free
        ctx.setattr(builtins, "open", mock_open())
        for slot_id in slot_ids:
            assert mgr_mixed.is_free(slot_id)
        assert mgr_mixed.lock_multi(slot_ids) is False


@pytest.mark.parametrize("slot_ids", [mock_slot_ids, mock_norm_slot_ids])
def test_release_multi(monkeypatch, mgr, slot_ids):
    """Release multiple devices."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_reg)
        for slot_id in slot_ids:
            assert mgr.lock(slot_id)
        mgr.release_multi(slot_ids)
        for slot_id in slot_ids:
            assert not mgr.is_locked(slot_id)


def test_scan_allfree(monkeypatch, mgr):
    """Scanning initial devices with all of them free."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_reg)
        mgr.scan()
        for slot_id in mock_slot_ids:
            assert mgr.is_free(slot_id)


def test_scan_occupied(monkeypatch, mgr_vfio):
    """Scanning initial devices with all of them occupied."""
    with monkeypatch.context() as ctx:
        ctx.setattr(psutil, "pid_exists", lambda x: True)
        ctx.setenv("PATH", mock_cmds_vfio)
        mgr_vfio.scan()
        for slot_id in mock_slot_ids:
            assert mgr_vfio.is_locked(slot_id)


def test_scan_dead(monkeypatch, mgr_vfio):
    """Scanning initial devices with all of them dead."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_vfio_bad)
        mgr_vfio.scan()
        for slot_id in mock_slot_ids:
            assert mgr_vfio.is_dead(slot_id)


@pytest.mark.parametrize("slot_ids", [mock_slot_ids, mock_norm_slot_ids])
def test_idle_sec_explicit(monkeypatch, mgr, slot_ids):
    """Idle time for single devices, after an explicit release."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_reg)
        # No prior initialization => assume not idle
        for slot_id in slot_ids:
            assert mgr.idle_sec(slot_id) == 0
        mgr.lock(slot_ids[0])
        ctx.setattr(time, "time", lambda: 1_000_000)
        mgr.release(slot_ids[0])
        ctx.setattr(time, "time", lambda: 1_000_023)
        assert mgr.idle_sec(slot_ids[0]) == 23
        for slot_id in slot_ids[1:]:
            assert mgr.idle_sec(slot_id) == 0


@pytest.mark.parametrize("slot_ids", [mock_slot_ids, mock_norm_slot_ids])
def test_idle_sec_implicit(monkeypatch, mgr, slot_ids):
    """Idle time for single devices, after an implicit release."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_reg)
        # No prior initialization => assume not idle
        for slot_id in slot_ids:
            assert mgr.idle_sec(slot_id) == 0
	    # Not a real PID, so an implicit release should happen
        mgr.lock(slot_ids[0], 123123123)
        ctx.setattr(time, "time", lambda: 1_000_000)
        assert mgr.idle_sec(slot_ids[0]) == 0
        ctx.setattr(time, "time", lambda: 1_000_023)
        assert mgr.idle_sec(slot_ids[0]) == 23

@pytest.mark.parametrize("slot_ids", [mock_slot_ids, mock_norm_slot_ids])
def test_idle_sec_all(monkeypatch, mgr, slot_ids):
    """Idle time for all devices."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_reg)
        # No prior initialization => assume not idle
        for slot_id in slot_ids:
            assert mgr.idle_sec(slot_id) == 0
        mgr.lock_multi(slot_ids)
        ctx.setattr(time, "time", lambda: 1_000_000)
        mgr.release(slot_ids[0])
        ctx.setattr(time, "time", lambda: 1_000_023)
        mgr.release(slot_ids[1])
        ctx.setattr(time, "time", lambda: 1_000_045)
        assert mgr.idle_sec(slot_ids[0]) == 45
        assert mgr.idle_sec(slot_ids[1]) == 22
        assert mgr.idle_sec_all() == 22
