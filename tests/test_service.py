# Author: Christian Kastner <ckk@kvr.at>
# License: MIT

# pylint: disable=unspecified-encoding


"""Tests for the ResourceManagerService component."""


import multiprocessing
import os
from pathlib import Path
import shlex
import socket
import sys
import threading
import time

import psutil
import pytest

sys.path.insert(0, "/usr/share/gpuenv-server/lib")
sys.path.insert(0, (Path(__file__).parent.parent / "lib").as_posix())
# pylint: disable-next=wrong-import-position
from service import ResourceManagerService


# These paths hold mock implementations of lspci, dmesg, lsof
mock_cmds_reg = (Path(__file__).parent / "mock_cmds" / "reg").as_posix()

# Two devices and various combinations of IDs
mock_slot_ids = ["06:00.0", "11:00.0"]


def communicate(socket_path, message, send_empty=False):
    """Trivial client for socket communication."""
    sock = socket.socket(family=socket.AF_UNIX, type=socket.SOCK_STREAM)
    sock.connect(socket_path)
    sock.settimeout(3.0)
    if message or send_empty:
        sock.send(message.encode())
        reply = sock.recv(1024).decode()
    sock.close()
    return reply


@pytest.fixture(name="server")
def fixture_server(monkeypatch, tmp_path):
    """A mock server managing two regular devices."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_reg)
        socket_path = (tmp_path / "pytest.socket").as_posix()
        return ResourceManagerService(slot_ids=mock_slot_ids, socket_path=socket_path)

def test_server_init(monkeypatch, tmp_path):
    """Server initialization."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_reg)
        socket_path = (tmp_path / "pytest.socket").as_posix()
        ResourceManagerService(slot_ids=mock_slot_ids, socket_path=socket_path)

def test_server_starts(monkeypatch, tmp_path):
    """Server starts up normally."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_reg)
        socket_path = (tmp_path / "pytest.socket").as_posix()
        server = ResourceManagerService(slot_ids=mock_slot_ids, socket_path=socket_path)
        process = multiprocessing.Process(target=server.run)
        process.start()
        time.sleep(0.5)
        exists = os.path.exists(socket_path)
        process.kill()
        process.join(timeout=3.0)
        assert exists

def test_server_stop(server):
    """Server shuts down on calling stop()."""
    # FIXME: This is broken. It will not terminate if the server doesn't stop.
    # The server needs to be switched from threading to multiprocessing.
    thread = threading.Thread(target=server.run)
    thread.start()
    server.stop()
    thread.join(timeout=3.0)
    assert not thread.is_alive()

def test_server_signal_stop(server):
    """Server shuts down on receiving SIGTERM."""
    process = multiprocessing.Process(target=server.run)
    process.start()
    time.sleep(1)
    process.terminate()
    process.join(timeout=3.0)
    is_alive = process.is_alive()
    process.kill()
    process.join(timeout=3.0)
    assert not is_alive


@pytest.mark.parametrize(
    "cmdstr,func,kwargs",
    [
        ("list_all", "list_all", {}),
        ("list_all None", "list_all", {"vfio": None}),
        ("list_all True", "list_all", {"vfio": True}),
        ("list_all False", "list_all", {"vfio": False}),
        ("normalize 06:00.0", "normalize", {"slot_id": "06:00.0"}),
        ("get_state 06:00.0", "get_state", {"slot_id": "06:00.0"}),
        ("is_free 06:00.0", "is_free", {"slot_id": "06:00.0"}),
        ("is_free_multi 06:00.0,11:00.0", "is_free_multi", {"slot_ids": ["06:00.0", "11:00.0"]}),
        ("is_locked 06:00.0", "is_locked", {"slot_id": "06:00.0"}),
        ("is_dead 06:00.0", "is_dead", {"slot_id": "06:00.0"}),
        ("lock 06:00.0", "lock", {"slot_id": "06:00.0"}),
        ("lock 06:00.0 12345", "lock", {"slot_id": "06:00.0", "pid": 12345}),
        ("lock_multi 06:00.0,11:00.0 12345", "lock_multi", {"slot_ids": ["06:00.0", "11:00.0"], "pid": 12345}),
        ("get_lock_pid 06:00.0", "get_lock_pid", {"slot_id": "06:00.0"}),
        ("release 06:00.0", "release", {"slot_id": "06:00.0"}),
        ("release_multi 06:00.0,11:00.0", "release_multi", {"slot_ids": ["06:00.0", "11:00.0"]}),
        ("mark_dead 06:00.0", "mark_dead", {"slot_id": "06:00.0"}),
    ],
)
def test_parse_command(server, cmdstr, func, kwargs):
    """Tests for converting strings to method names and kwargs."""
    args = shlex.split(cmdstr)
    assert server.parse_command(args) == (func, kwargs)

@pytest.mark.parametrize(
    "cmdstr,exc",
    [
        ("lock",IndexError),
        ("lock 06:00.0 11:00.0",ValueError),
        ("lock 06:00.0,11:00.0",ValueError),
        ("lock 06:00 foo",ValueError),
    ],
)
def test_parse_command_invalid(server, cmdstr, exc):
    """Tests for catching errors when converting strings to method names and kwargs."""
    args = shlex.split(cmdstr)
    with pytest.raises(exc):
        server.parse_command(args)


@pytest.mark.parametrize(
    "cmdstr,expected",
    [
        # Must send at least one byte
        ("\n", "400 Empty request"),
        ("bogus", "400 Unsupported method"),
        ("lock 06:00.0 somepid", "400 Malformed command"),
        ("lock 99:00.0", "405 Unknown slot"),
    ],
)
def test_server_communicate_errors(monkeypatch, server, cmdstr, expected):
    """Erroneous commands return errors."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_reg)
        process = multiprocessing.Process(target=server.run)
        process.start()
        time.sleep(1)
        try:
            reply = communicate(server.socket_path, cmdstr, send_empty=True)
        except socket.timeout:
            reply = None
        process.kill()
        process.join(timeout=3.0)
        assert reply == expected


@pytest.mark.parametrize(
    "cmdstr,expected",
    [
        ("list_all", "200 0000:06:00.0,0000:11:00.0"),
        ("list_all None", "200 0000:06:00.0,0000:11:00.0"),
        ("list_all False", "200 0000:06:00.0,0000:11:00.0"),
        ("list_all True", "200 "),
        ("normalize 06:00.0", "200 0000:06:00.0"),
        ("get_state 06:00.0", "200 FREE"),
        ("is_free 06:00.0", "200 True"),
        ("is_free_multi 06:00.0,11:00.0", "200 True"),
        ("is_locked 06:00.0", "200 False"),
        ("mark_dead 06:00.0", "200"),
    ],
)
def test_server_communicate_basic(monkeypatch, server, cmdstr, expected):
    """Server communication using simple one-shot commands."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_reg)
        process = multiprocessing.Process(target=server.run)
        process.start()
        # Process needs a bit of time for the signal handler to register
        # Without this, test failures have been observed in single-CPU environments
        time.sleep(0.5)
        try:
            reply = communicate(server.socket_path, cmdstr)
        except socket.timeout:
            reply = None
        process.kill()
        process.join(timeout=3.0)
        assert reply == expected


def test_server_communicate_series(monkeypatch, server):
    """Server communication with multiple commands."""
    with monkeypatch.context() as ctx:
        ctx.setenv("PATH", mock_cmds_reg)
        # 1234512345 is not a real PID, so we need to prevent implicit release of the lock
        ctx.setattr(psutil, "pid_exists", lambda x: True)
        process = multiprocessing.Process(target=server.run)
        process.start()
        time.sleep(1)
        reply1 = reply2 = reply3 = None
        try:
            reply1 = communicate(server.socket_path, "lock 06:00.0 1234512345")
            reply2 = communicate(server.socket_path, "lock 06:00.0")
            reply3 = communicate(server.socket_path, "get_lock_pid 06:00.0")
            reply4 = communicate(server.socket_path, "release 06:00.0")
        except socket.timeout:
            pass
        process.kill()
        process.join(timeout=3.0)
        assert reply1 == "200 True"
        assert reply2 == "200 False"
        assert reply3 == "200 1234512345"
        assert reply4 == "200"
