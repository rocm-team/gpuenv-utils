# gpuenv-utils

This set of utilities aims to monitor and to facilitate cooperative access to
GPUs. The primary intent is to let various concurrently running test workers,
build workers, and porter box environments have exclusive control over one or
more GPUs for the duration of their work.

This is achieved through a simple form of advisory locking. The server is
initialized with a set of resources, and clients communicate with the server to
acquire and release locks on those resources.

This generally depends on the cooperation of all potential clients. There is
(yet) no way to prevent an arbitrary process to access the GPU outside of this
scheme. However, for VFIO-assigned devices, access by an arbitrary process is
at least detected, and a lock is implicitly granted to its PID.

In addition to the above, devices are also monitored for responsiveness. This
happens by checking `dmesg` for error markers.


## Resources

Currently, the only supported resource is a GPU, identified by PCI slot ID.
This works for both standard and VFIO-assigned devices (for QEMU pass-through).


## Utilities

* `gpuenv-server`: A service that listens for client requests on the UNIX
  socket `/run/gpuenv.socket`.

* `gpuenv-cli`: A minimal utility for interacting with the server from within a
  shell. See `gpuenv-cli -h` for more information. To communicate with the
  server, users must be in group `render`:

  ```shell
  $ sudo gpasswd -a <user> render
  ```

## Services

* `gpuenv-auto-uptime`: A service that shuts down a host when it has been
  idling for more than 15 minutes, and reboots a host when an unresponsive GPU
  is encountered.

  A host is considered idle when all of the following conditions are met:

  * All GPUs have been idle for more than 15 minutes
  * Sum(1m, 5m, 15m load average) < 0.001
  * No users are logged in.

  This service is not enabled by default. To enable it, simply run:

  ```shell
  $ sudo systemctl enable --now gpuenv-auto-uptime.timer
  ```


## Device responsiveness

If, for a particular PCI splot ID, `dmesg` presents lines which indicate an
error, then the device is marked as "dead" and can no longer be locked. Errors
are currently indicated by the following substrings:

* `device inaccessible`
* `Refused to change power state`
* `Unable to change power state`

There have been cases where these messages turned out to be false positives.
The file `/etc/gpuenv-utils/markers.notdead` can be used as a workaround: any
`dmesg` lines containing a string listed in that file will be ignored.


## Examples

```shell

# Our example cards
$ lspci | grep VGA
03:00.0 VGA compatible controller [...]
06:00.0 VGA compatible controller [...]

# Check if the device is available
$ gpuenv-cli is_free 03:00.0; echo $?
0

# Acquire a lock on the device
$ gpuenv-cli lock 03:00.0; echo $?
0
# Alternatively: lock with a PID. Future calls to is_free() or is_locked() will
# release the lock if no process with such a PID exists any more.
$ gpuenv-cli lock --pid 12345 03:00.0; echo $?
0

# Device is reported as occupied
$ gpuenv-cli is_free 03:00.0; echo $?
1

# Future lock attempts fail
$ gpuenv-cli lock 03:00.0; echo $?
1

# Release the device
$ gpuenv-cli release 03:00.0

# The device is reported as free again
$ gpuenv-cli is_free 03:00.0; echo $?
0

# Check if multiple devices are available
$ gpuenv-cli is_free_multi 03:00.0,06:00.0; echo $?
0

# Acquire a lock on multiple devices. Fails if at least one lock fails
$ gpuenv-cli lock_multi 03:00.0,06:00.0; echo $?
0

# Release multiple locks
$ gpuenv-cli release_multi 03:00.0,06:00.0

# (Output values below are exemplary and assume no other process are accessing the GPU)
# Check time a GPU has been idle
$ gpuenv-cli lock 03:00.0
$ gpuenv-cli idle_secs 03:00.0
0
$ gpuenv-cli release 03:00.0; sleep 60
$ gpuenv-cli idle_secs 03:00.0
60
# For all GPUs: overall idle time is minimum of individual idle times
$ gpuenv-cli lock 06:00.0; gpuenv-cli release 06:00.0; sleep 15
$ gpuenv-cli idle_secs 03:00.0; gpuenv-cli idle_secs 06:00.0
60
15
$ gpuenv-cli idle_secs_all
15
$ gpuenv-cli lock 03:00.0
$ gpuenv-cli idle_secs_all
0

# List all devices
$ gpuenv-cli list_all
0000:03:00.0
0000:06:00.0

# List all (non-)VFIO devices
$ gpuenv-cli list_all --(no-)vfio
```
