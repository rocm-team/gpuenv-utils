#!/usr/bin/python3

# Author: Christian Kastner <ckk@kvr.at>
# License: MIT


"""Shuts down idle machines, reboots machines with unresponsive GPUs."""


import argparse
import logging
import os
import subprocess
import sys

import psutil


def host_is_idle() -> bool:
    """Check whether a host has been idling, and thus could be shut down.

    A host is considered to be idle when:
        (1) No user is logged in
        (2) 1/5/15minload averages sum to < 0.001
            The 0.001 is to account for short-lived periodic background tasks
            that briefly bounce load
        (3) GPUs have been free (= not locked) for >=15mins
    """
    load = psutil.getloadavg()
    users = psutil.users()
    try:
        out = subprocess.check_output(["gpuenv-cli", "idle_sec_all"], text=True)
    except subprocess.CalledProcessError as err:
        logging.error("Failed to check GPU idle time: %s", err)
        return False
    try:
        gpu_idle_sec = int(out.strip())
    except ValueError as err:
        logging.error("Failed to determine GPU idle time: %s", err)

    logging.debug(
        "Users: %s, Sum(load): %s, GPU-idle: %ss", len(users), sum(load), gpu_idle_sec
    )
    if gpu_idle_sec > 15 * 60 and sum(load) < 0.001 and len(users) == 0:
        return True
    return False


def host_needs_reboot() -> bool:
    """Check whether there are unresponsive GPUs on the host."""
    try:
        out = subprocess.check_output(["gpuenv-cli", "list_all"], text=True)
    except subprocess.CalledProcessError as err:
        logging.error("Failed to list all GPUs: %s", err)
        return False

    for slot_id in out.strip().split("\n"):
        try:
            out = subprocess.check_call(["gpuenv-cli", "is_dead", slot_id])
        except subprocess.CalledProcessError:
            pass
        else:
            logging.fatal("Dead GPU on slot %s, host needs reboot.", slot_id)
            return True
    logging.info("All GPUs alive")
    return False


def main() -> None:
    """Main"""
    parser = argparse.ArgumentParser(
        description="Shutdown idle machines, reboot broken ones."
    )
    parser.add_argument(
        "--shutdown-idle",
        action="store_true",
        help="Shutdown after idling for approx. 15 minutes.",
    )
    parser.add_argument(
        "--rtcwake-date",
        metavar="timespec",
        help="When shutting down, set an RTC wakeup --date per the given timespec.",
    )
    parser.add_argument(
        "--reboot-on-dead",
        action="store_true",
        help="Reboot a machine when a dead GPU is encountered.",
    )
    parser.add_argument(
        "--systemd",
        action="store_true",
        help="Run with socket activation by systemd.",
    )
    args = parser.parse_args()

    if args.systemd:
        logging.basicConfig(
            level=logging.DEBUG,
            format="%(message)s",
        )
    else:
        logging.basicConfig(
            level=logging.DEBUG,
            format="%(asctime)s %(levelname)s %(message)s",
        )
    logging.info("Checking for idle or dead GPUs")

    if os.getuid() != 0:
        logging.critical("This script must be run as root.")
        sys.exit(1)

    if args.shutdown_idle and host_is_idle():
        if args.rtcwake_date:
            cmd = ["rtcwake", "-m", "off", "--date", args.rtcwake_date]
        else:
            cmd = ["poweroff"]
        try:
            subprocess.check_call(cmd)
        except subprocess.CalledProcessError as err:
            logging.critical("Error powering off: %s", err)
            sys.exit(1)

    if args.reboot_on_dead and host_needs_reboot():
        if len(psutil.users()) > 0:
            logging.warning("Not rebooting, user(s) logged in")
        else:
            logging.info("Requesting reboot")
            cmd = ["rtcwake", "-m", "off", "--date", '+30sec']
            try:
                subprocess.check_call(cmd)
            except subprocess.CalledProcessError as err:
                logging.critical("Error rebooting: %s", err)
                sys.exit(1)


if __name__ == "__main__":
    main()
